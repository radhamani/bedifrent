<?php
    require_once './phpmailer/PHPMailerAutoload.php';


    if (isset($_POST['name']) && isset($_POST['email']) && isset($_POST['phone']) && isset($_POST['message'])){

        $name = isset($_POST['name']) ? $_POST['name'] : '';
        $email = isset($_POST['email']) ? $_POST['email'] : '';
        $phone = isset($_POST['phone']) ? $_POST['phone'] : '';
        $message = isset($_POST['message']) ? $_POST['message'] : '';

        $result['success'] = false;
        $result['message'] = "Error Sending Email.";

        if(!empty($name) && !empty($email)&& !empty($phone) && !empty($message)){
            $msg = "You have received a new query from Be Difrent as below- <br/><br/>";
            $msg .= 'Name: '. $name ."<br>";
            $msg .= 'Email: '. $email ."<br>";
            $msg .= 'Phone: '. $phone ."<br>";
            $msg .= 'Message: '. $message ."<br>";

            $phpmailer = new PHPMailer();
            $phpmailer->CharSet = 'UTF-8';

            $phpmailer->isMail();

            $phpmailer->SetFrom("bedifrent12@gmail.com", "Be Difrent");

            //$phpmailer->AddAddress('Karen.Cleale@bedifrent.com');
			//$phpmailer->AddAddress('Rachel.Murphy@bedifrent.com');
            $phpmailer->AddAddress('minal@thinkoverit.com');


            $phpmailer->Subject = "Contact Query";
            $phpmailer->MsgHTML($msg);

            if(!$phpmailer->Send()) {
                $result['success'] = false;
                $result['message'] = $phpmailer->ErrorInfo;
            }else{
                $result['success'] = true;
                $result['message'] = 'Thanks for the contacting us. Our team will get in touch with you as early as possible.';
            }
        }else{
            $result['success'] = false;
            $result['message'] = 'Please fill all fields with valid data.';
        }
        echo json_encode($result);
        exit;
    }
?>
