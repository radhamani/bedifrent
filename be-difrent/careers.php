<?php
    require_once './phpmailer/PHPMailerAutoload.php';
    require_once './phpmailer/class.phpmailer.php';

    if (isset($_POST['name']) && isset($_POST['email']) 
        && isset($_POST['phone']) && isset($_POST['position']) 
        && isset($_POST['experience']) && isset($_POST['whyjoin'])){
        
        $name = isset($_POST['name']) ? $_POST['name'] : '';
        $email = isset($_POST['email']) ? $_POST['email'] : '';
        $phone = isset($_POST['phone']) ? $_POST['phone'] : '';
        $position = isset($_POST['position']) ? $_POST['position'] : '';
        $experience = isset($_POST['experience']) ? $_POST['experience'] : '';
        $whyjoin = isset($_POST['whyjoin']) ? $_POST['whyjoin'] : '';

        if(!empty($name) && !empty($email)&& !empty($phone) && !empty($position) && !empty($experience) && !empty($whyjoin)){
            $msg = "You have received a new query from Be Difrent as below- <br/><br/>";
            $msg .= 'Name: '. $name ."<br>";
            $msg .= 'Email: '. $email ."<br>";
            $msg .= 'Phone: '. $phone ."<br>";
            $msg .= 'Position: '. $position ."<br>";
            $msg .= 'Experience: '. $experience ."<br>";
            $msg .= 'Why do you want to join us?: '. $whyjoin ."<br>";

            $phpmailer = new PHPMailer();
            $phpmailer->CharSet = 'UTF-8';
            
            $phpmailer->isMail();
            
            $phpmailer->SetFrom("bedifrent12@gmail.com", "Be Difrent");

            //$phpmailer->AddAddress('Karen.Cleale@bedifrent.com');
			//$phpmailer->AddAddress('Rachel.Murphy@bedifrent.com');
            $phpmailer->AddAddress('minal@thinkoverit.com');
            
            
            $phpmailer->Subject = "Join Our Team";
            $phpmailer->MsgHTML($msg);

       
            $resume = $_FILES['resume']['name'];
            if ( 0 < $_FILES['resume']['error']) {
               
            } else {
                $phpmailer->AddAttachment($_FILES['resume']['tmp_name'], $_FILES['resume']['name']);
            } 
            
            if(!$phpmailer->Send()) {
                $result['success'] = false;
                $result['message'] = "Failed to send an email.";
            }else{            
                $result['success'] = true;
                $result['message'] = 'Thanks for the contacting us. Our team will get in touch with you as early as possible.';
            }
        }else{
            $result['success'] = false;
            $result['message'] = 'Please fill all fields with valid data.';
        }
        echo json_encode($result);
        exit;
    }
?>