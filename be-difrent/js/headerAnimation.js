function canvasAnimate(canvas) {
  var mouseX = mouseY = mouseOldX = mouseOldY = 0,
    C,c,viewWidth,viewHeight,
  	triW = 50,
    triH = 60,
    neighbours = ["side", "top", "bottom"],
    speedTrailAppear = .5,
    speedTrailDisappear = 20,
    speedTriOpen = 0.7,
    trailMaxLength = 18,
    trailIntervalCreation = 120,
    delayBeforeDisappear = 10,
    cols, rows,
    tris,
    triangle,
    randomAlpha = true,
    colors = [
    //oranges
    '#eb9000', '#f6b400', '#f5cb00', '#fbde1a', '#feeb31', '#fff163',
    //reds
    '#440510', '#8a0a08', '#c11e1f', '#e52822', '#f43d0d', '#fb5c28', '#ff8547',
    //greens
    '#05391e', '#004b25', '#006d3c', '#059849', '#15ae44', '#52b933', '#78c340', '#b2cd53',
    //blues
    '#0c1a36', '#01235d', '#0b3f7a', '#0561a6', '#007ecb', '#32b2fa', '#54cefc', '#91dffa'
  ];
  Triangle = function(pos, column, row) {
    var thisTri = this;
    this.c=c;
    this.selectedForTrail = false;
    this.pos = pos;
    this.col = column;
    this.row = row;
    this.alpha = this.tAlpha = 1;
    this.opened = false;
    this.opening = false;
    this.closing = false;
    this.isLeft = (this.pos % 2);
    // this.color = colors[Math.floor(Math.random() * colors.length)];
    // this.rgb = hexToRgb(this.color);
    this.tX1 = (this.isLeft) ? (this.col + 1) * triW : this.col * triW;
    this.tX2 = (this.isLeft) ? this.col * triW : (this.col + 1) * triW;
    this.tX3 = (this.isLeft) ? (this.col + 1) * triW : this.col * triW;
    this.x1 = this.tX1;
    this.x2 = this.tX1;
    this.x3 = this.tX1;
    this.tY1 = .5 * this.row * triH;
    this.tY2 = .5 * (this.row + 1) * triH;
    this.tY3 = .5 * (this.row + 2) * triH;
    this.y1 = this.tY1;
    this.y2 = this.tY1;
    this.y3 = this.tY1;
    this.tweenClose, this.tweenOpen;
    this.draw = function() {
      var mygradient1= this.c.createLinearGradient(0,0,400,400);
      mygradient1.addColorStop(0,"#c37a82");
      mygradient1.addColorStop(0.2,"#9fbe57");
      mygradient1.addColorStop(0.4,"#d3535e");
      mygradient1.addColorStop(0.6,"#24bec0");
      mygradient1.addColorStop(0.8,"#0000FF");
      this.c.beginPath();
      this.c.strokeStyle = mygradient1;
      this.c.lineWidth=1.5;
      // this.c.fillStyle ='rgba(' + this.rgb.r + ',' + this.rgb.g + ',' + this.rgb.b + ',' + this.alpha + ')';
      this.c.moveTo(this.y1, this.x1);
      this.c.lineTo(this.y2, this.x2);
      this.c.lineTo(this.y3, this.x3);
      this.c.lineTo(this.y1, this.x1);
      this.c.stroke();
      //c.fill();
    }
         this.open = function(direction, targetSpeed, targetAlpha, targetDelay) {
             if (this.tweenClose) this.tweenClose.kill();
             this.opening = true;
             this.direction = direction || "top";
             this.delay = targetDelay || 0;
             this.tAlpha = targetAlpha;
             this.tSpeed = targetSpeed || 1.5;
             if (this.direction == "side") {
               this.x1 = this.x2 = this.x3 = this.tX1;
               this.y1 = this.tY1;
               this.y2 = this.tY2;
               this.y3 = this.tY3;
             } else if (this.direction == "top") {
               this.x1 = (this.tX2 + this.tX3) / 2;
               this.x2 = this.tX2;
               this.x3 = this.tX3;
               this.y1 = (this.tY2 + this.tY3) / 2;
               this.y2 = this.tY2;
               this.y3 = this.tY3;
             } else if (this.direction == "bottom") {
               this.x1 = this.tX1;
               this.x2 = this.tX2;
               this.x3 = (this.tX1 + this.tX2) / 2;
               this.y1 = this.tY1;
               this.y2 = this.tY2;
               this.y3 = (this.tY1 + this.tY2) / 2;
             }

             this.tweenOpen = TweenMax.to(this, this.tSpeed, {
               x1: this.tX1,
               x2: this.tX2,
               x3: this.tX3,
               y1: this.tY1,
               y2: this.tY2,
               y3: this.tY3,
               alpha: this.tAlpha,
               ease: Strong.easeInOut,
               delay: this.delay,
               onComplete:openComplete,
               onCompleteParams:[thisTri]
             });
         }
         this.close = function(direction, targetSpeed, targetDelay) {
             this.check=false;
             this.direction = direction || "top";
             this.delay = targetDelay || 1;
             this.tSpeed = targetSpeed || .8;
             this.opened = false;
             this.closing = true;
             var closeX1, closeX2, closeX3, closeY1, closeY2, closeY3;
             if (this.direction == "side") {
               closeX1 = closeX2 = closeX3 = this.tX1;
               closeY1 = this.tY1;
               closeY2 = this.tY2;
               closeY3 = this.tY3;
             } else if (this.direction == "top") {
               closeX1 = (this.tX2 + this.tX3) / 2;
               closeX2 = this.tX2;
               closeX3 = this.tX3;
               closeY1 = (this.tY2 + this.tY3) / 2;
               closeY2 = this.tY2;
               closeY3 = this.tY3;
             } else if (this.direction == "bottom") {
               closeX1 = this.tX1;
               closeX2 = this.tX2;
               closeX3 = (this.tX1 + this.tX2) / 2;
               closeY1 = this.tY1;
               closeY2 = this.tY2;
               closeY3 = (this.tY1 + this.tY2) / 2;
             }
             if (this.tweenClose) this.tweenClose.kill();
             this.tweenClose = TweenMax.to(this, this.tSpeed, {
               x1: closeX1,
               x2: closeX2,
               x3: closeX3,
               y1: closeY1,
               y2: closeY2,
               y3: closeY3,
               alpha: this.tAlpha,
               ease: Strong.easeInOut,
               delay: this.delay,
               onComplete:closeComplete,
               onCompleteParams:[thisTri]
             });
         }
       }

  function openComplete(tri){
    tri.opened = true;
    tri.opening = false;
    tri.closing = false;
  }
  function closeComplete(tri){
    tri.opened = false;
    tri.opening = false;
    tri.closing = false;
  }
  function unselectTris() {
    for (var i = 0; i < tris.length; i++) {
      tris[i].selectedForTrail = false;
    }
  }
  function createTrail() {
    unselectTris();
    var currentTri;
    var trailLength = Math.floor(Math.random() * trailMaxLength - 2) + 2;
    var index = Math.round(Math.random() * tris.length);
    startTri = tris[index];
    currentTri = {
      tri: startTri,
      openDir: "side",
      closeDir: "side"
    };
    for (var i = 0; i < trailLength; i++) {
      var o = null;
      if (currentTri.tri) {
         o = getNeighbour(currentTri.tri);
      }
      if (o != null) {
        var opacity;
        if (randomAlpha){
          opacity = (Math.random()<.8) ? Math.random()*.5 : 1;
        }else{
          opacity = 1;
        }
        currentTri.tri.closeDir = o.openDir;
        currentTri.tri.open(currentTri.openDir, speedTriOpen, opacity, i* speedTrailAppear);
    		currentTri.tri.close(currentTri.closeDir, 1, delayBeforeDisappear + i*speedTrailDisappear);
        currentTri = o;
      } else {
        if(currentTri.tri){
          currentTri.tri.open(currentTri.openDir, speedTriOpen, opacity, (i+1)* speedTrailAppear);
      		currentTri.tri.close(currentTri.closeDir, 1, delayBeforeDisappear + (i+1)*speedTrailDisappear);
        }
        break;
      }
    }
  }
  function getNeighbour(t) {
    shuffleArray(neighbours);
    for (var i = 0; i < neighbours.length; i++) {
      if (neighbours[i] == "top") {
          return {
            tri: tris[t.pos - cols],
            openDir: "top",
            closeDir: "top"
          };
      }
    }
    return null;
  }
  function draw() {
    c.clearRect(0, 0, C.width, C.height);
    for (var i = 0; i < tris.length; i++) {
      tris[i].draw();
    }
  }
  function handleResize() {
    viewWidth = C.width = C.scrollWidth;
    viewHeight = C.height  = C.scrollHeight;
    rectCanvas = C.getBoundingClientRect();
    start();
  }
  // function hexToRgb(hex) {
  //   var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
  //   return result ? {
  //     r: parseInt(result[1], 16),
  //     g: parseInt(result[2], 16),
  //     b: parseInt(result[3], 16)
  //   } : null;
  // }
  function shuffleArray(o) {
    for (var j, x, i = o.length; i; j = Math.floor(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
    return o;
  }
  function initCanvasOne(canvasId) {
    C = document.getElementById(canvasId);
    c = C.getContext('2d');
    viewWidth = C.width = C.scrollWidth;
    viewHeight = C.height  = C.scrollHeight;
    initParams();
    window.addEventListener("resize", handleResize);
    TweenLite.ticker.addEventListener("tick", draw);
    handleResize();
  }
  function initParams(){
    cols = 11;
    cols = (cols % 2) ? cols : cols - 1; // => keep it odd
    rows = 10;
    tris = [];
  }
  function initGrid() {
    for (var j = 0; j < rows; j++) {
      for (var i = 0; i < cols; i++) {
          var pos = i + (j * cols);
           triangle = new Triangle(pos, i, j);
          if((i==1 && j == 0) ||(i==0 && j == 2) || (i==0 && j == 3) || (i==0 && j == 4) || (i==1 && j == 2) || (i==1 && j == 3)|| (i==1 && j == 6) || (i==2 && j == 1) || (i==2 && j == 2) || (i==2 && j == 3)|| (i==2 && j == 4) || (i==2 && j == 5) || (i==2 && j == 6) || (i==2 && j == 7) || (i==3 && j == 1) || (i==3 && j == 2) || (i==4 && j == 4) || (i==5 && j == 5) || (i==5 && j == 3)){
            tris.push(triangle);
          }
          triangle.draw();
      }
    }
  }
  var interval;
  function start(){
    if (interval) clearInterval(interval);
    initParams();
    initGrid();
    interval = setInterval(createTrail, trailIntervalCreation);
    createTrail();
  }
  function kill(){
    if (interval) clearInterval(interval);
    for (var i = 0; i < tris.length; i++) {
      TweenLite.killTweensOf(tris[i]);
      tris[i].alpha = 0;
    }
  }
  initCanvasOne(canvas);
start();
}
