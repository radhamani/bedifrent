//Animation Services

var attribute_triangle_one = [
    {"color" : "#dcf0f7", "transitionX" : 550 , "transitionY" : 610 , "tranX" : 445 , "tranY" : 585 ,  "rotate" : 270,  "arc_open": 'M0,-20.808957251439086L24.028114141347544,20.808957251439086 -24.028114141347544,20.808957251439086Z' },
    {"color" : "#76cedf", "transitionX" : 340 , "transitionY" : 32 , "tranX" : 220 , "tranY" : 100 ,  "rotate" : 90,  "arc_open": 'M0,-25.48566367270684L29.42830956382712,25.48566367270684 -29.42830956382712,25.48566367270684Z' },
    {"color" : "#54c9f3", "transitionX" : 675 , "transitionY" : 268 , "tranX" : 490 , "tranY" : 280 ,  "rotate" : 90,  "arc_open": 'M0,-25.48566367270684L29.42830956382712,25.48566367270684 -29.42830956382712,25.48566367270684Z' },
    {"color" : "#d8f1f6", "transitionX" : 420 , "transitionY" : 84 , "tranX" : 220 , "tranY" : 100 ,  "rotate" : 90,  "arc_open": 'M0,-32.90185032381231L37.99178428257963,32.90185032381231 -37.99178428257963,32.90185032381231Z' },
    {"color" : "#a4e2f9", "transitionX" : 590 , "transitionY" : 560 , "tranX" : 490 , "tranY" : 440 ,  "rotate" : 90,  "arc_open": 'M0,-36.04217121202131L41.61791450287817,36.04217121202131 -41.61791450287817,36.04217121202131Z' },
    {"color" : "#54c9f3", "transitionX" : 545 , "transitionY" : 295 , "tranX" : 370 , "tranY" : 400 ,  "rotate" : 270,  "arc_open": 'M0,-48.80133051882096L56.35092262370637,48.80133051882096 -56.35092262370637,48.80133051882096Z' },
    {"color" : "#0ea8c6", "transitionX" : 265 , "transitionY" : 122 , "tranX" : 150 , "tranY" : 290 ,  "rotate" : 270,  "arc_open": 'M0,-65.80370064762462L75.98356856515926,65.80370064762462 -75.98356856515926,65.80370064762462Z' },
    {"color" : "#c4e7f1", "transitionX" : 525 , "transitionY" : 465 , "tranX" : 380 , "tranY" : 405 ,  "rotate" : 270,  "arc_open": 'M0,-69.01550348156863L79.69223902668242,69.01550348156863 -79.69223902668242,69.01550348156863Z' },
    {"color" : "#78b8c7", "transitionX" : 537 , "transitionY" : 370 , "tranX" : 250 , "tranY" : 370 ,  "rotate" : 90,  "arc_open": 'M0,-90.70414177948088L104.73612134599453,90.70414177948088 -104.73612134599453,90.70414177948088Z' },
    {"color" : "#54c9f3", "transitionX" : 325 , "transitionY" : 250 , "tranX" : 250 , "tranY" : 370 ,  "rotate" : 90,  "arc_open": 'M0,-97.60266103764192L112.70184524741273,97.60266103764192 -112.70184524741273,97.60266103764192Z' },
    {"color" : "#3caed1", "transitionX" : 265 , "transitionY" : 413 , "tranX" : 80 , "tranY" : 630 ,  "rotate" : 270,  "arc_open": 'M0,-123.10745130262292L142.15224029763618,123.10745130262292 -142.15224029763618,123.10745130262292Z' },
    { "color" : "#19879f", "transitionX" : 272 ,  "transitionY" : 575 , "tranX" : -500 ,  "tranY" : 397 ,   "rotate" : 90 ,  "arc_open": 'M0,-152.20440056461487L175.75050327565205,152.20440056461487 -175.75050327565205,152.20440056461487Z' }
];
var attribute_triangle_two = [
    { "color" : "#f6e2ea", "transitionX" : 198 ,  "transitionY" : 530 , "tranX" : 290 , "tranY" : 450 , "rotate" : 90 ,  "arc_open": 'M0,-20.808957251439086L24.028114141347544,20.808957251439086 -24.028114141347544,20.808957251439086Z' },
    { "color" : "#a6a8c7", "transitionX" : 340 ,  "transitionY" : 95 , "tranX" : 460 , "tranY" : 28 , "rotate" : 90 ,  "arc_open": 'M0,-25.48566367270684L29.42830956382712,25.48566367270684 -29.42830956382712,25.48566367270684Z' },
    { "color" : "#b9667c", "transitionX" : 187 ,  "transitionY" : 164 , "tranX" : 300 , "tranY" : 254 , "rotate" : 270 ,  "arc_open": 'M0,-25.48566367270684L29.42830956382712,25.48566367270684 -29.42830956382712,25.48566367270684Z' },
    { "color" : "#f0e5f0", "transitionX" : 318 ,  "transitionY" : 53 , "tranX" : 530 , "tranY" : 115 , "rotate" : 270 ,  "arc_open": 'M0,-32.90185032381231L37.99178428257963,32.90185032381231 -37.99178428257963,32.90185032381231Z' },
    { "color" : "#afa3c0", "transitionX" : 151 ,  "transitionY" : 473 , "tranX" : 370 , "tranY" : 453 , "rotate" : 270,  "arc_open": 'M0,-36.04217121202131L41.61791450287817,36.04217121202131 -41.61791450287817,36.04217121202131Z' },
    { "color" : "#a35ba4", "transitionX" : 248 ,  "transitionY" : 235 , "tranX" : 420 , "tranY" : 280 , "rotate" : 90 ,  "arc_open": 'M0,-48.80133051882096L56.35092262370637,48.80133051882096 -56.35092262370637,48.80133051882096Z' },
    { "color" : "#675188", "transitionX" : 475 ,  "transitionY" : 90 , "tranX" : 650 , "tranY" : 150 , "rotate" : 90 ,  "arc_open": 'M0,-65.80370064762462L75.98356856515926,65.80370064762462 -75.98356856515926,65.80370064762462Z' },
    { "color" : "#d0d1e1", "transitionX" : 315 ,  "transitionY" : 435 , "tranX" : 430 , "tranY" : 275 , "rotate" : 90 ,  "arc_open": 'M0,-69.01550348156863L79.69223902668242,69.01550348156863 -79.69223902668242,69.01550348156863Z' },
    { "color" : "#d6a5b2", "transitionX" : 258 ,  "transitionY" : 310 , "tranX" : 558 , "tranY" : 390 , "rotate" : 270 ,  "arc_open": 'M0,-90.70414177948088L104.73612134599453,90.70414177948088 -104.73612134599453,90.70414177948088Z' },
    { "color" : "#b9667c", "transitionX" : 478 ,  "transitionY" : 185 , "tranX" : 558 , "tranY" : 390 , "rotate" : 270 ,  "arc_open": 'M0,-97.60266103764192L112.70184524741273,97.60266103764192 -112.70184524741273,97.60266103764192Z' },
    { "color" : "#a35ba4", "transitionX" : 533 ,  "transitionY" : 345 , "tranX" : 720 , "tranY" : 460 , "rotate" : 90 ,  "arc_open": 'M0,-123.10745130262292L142.15224029763618,123.10745130262292 -142.15224029763618,123.10745130262292Z' },
    { "color" : "#675188", "transitionX" : 527 ,  "transitionY" : 510 , "tranX" : 1300 ,  "tranY" : 510 , "rotate" : 270 ,  "arc_open": 'M0,-152.20440056461487L175.75050327565205,152.20440056461487 -175.75050327565205,152.20440056461487Z' }
];
var attribute_triangle_three = [
    { "color" : "#fbece0", "transitionX" : 615 ,  "transitionY" : 435 , "tranX" : 600 , "tranY" : 495 , "rotate" : 270 ,  "arc_open": 'M0,-20.808957251439086L24.028114141347544,20.808957251439086 -24.028114141347544,20.808957251439086Z' },
    { "color" : "#f2bc94", "transitionX" : 355 ,  "transitionY" : 32 , "tranX" : 320 , "tranY" : 100 , "rotate" : 90 ,  "arc_open": 'M0,-25.48566367270684L29.42830956382712,25.48566367270684 -29.42830956382712,25.48566367270684Z' },
    { "color" : "#eba24a", "transitionX" : 555 ,  "transitionY" : 199 , "tranX" : 535 , "tranY" : 250 , "rotate" : 90 ,  "arc_open": 'M0,-25.48566367270684L29.42830956382712,25.48566367270684 -29.42830956382712,25.48566367270684Z' },
    { "color" : "#f5ec91", "transitionX" : 425 ,  "transitionY" : 162 , "tranX" : 325 , "tranY" : 102 , "rotate" : 90 ,  "arc_open": 'M0,-32.90185032381231L37.99178428257963,32.90185032381231 -37.99178428257963,32.90185032381231Z' },
    { "color" : "#f5da9e", "transitionX" : 635 ,  "transitionY" : 473 , "tranX" : 535 , "tranY" : 410 , "rotate" : 90 ,  "arc_open": 'M0,-36.04217121202131L41.61791450287817,36.04217121202131 -41.61791450287817,36.04217121202131Z' },
    { "color" : "#e9d94a", "transitionX" : 495 ,  "transitionY" : 273 , "tranX" : 455 , "tranY" : 373 , "rotate" : 270 ,  "arc_open": 'M0,-48.80133051882096L56.35092262370637,48.80133051882096 -56.35092262370637,48.80133051882096Z' },
    { "color" : "#eba24a", "transitionX" : 270 ,  "transitionY" : 125 , "tranX" : 205 , "tranY" : 255 , "rotate" : 270 ,  "arc_open": 'M0,-65.80370064762462L75.98356856515926,65.80370064762462 -75.98356856515926,65.80370064762462Z' },
    { "color" : "#faeac8", "transitionX" : 475 ,  "transitionY" : 435 , "tranX" : 425 , "tranY" : 380 , "rotate" : 270 ,  "arc_open": 'M0,-69.01550348156863L79.69223902668242,69.01550348156863 -79.69223902668242,69.01550348156863Z' },
    { "color" : "#f1ba90", "transitionX" : 482 ,  "transitionY" : 342 , "tranX" : 290 , "tranY" : 338 , "rotate" : 90 ,  "arc_open": 'M0,-90.70414177948088L104.73612134599453,90.70414177948088 -104.73612134599453,90.70414177948088Z' },
    { "color" : "#e9d94a", "transitionX" : 270 ,  "transitionY" : 218 , "tranX" : 290 , "tranY" : 338 , "rotate" : 90 ,  "arc_open": 'M0,-97.60266103764192L112.70184524741273,97.60266103764192 -112.70184524741273,97.60266103764192Z' },
    { "color" : "#eba24a", "transitionX" : 210 ,  "transitionY" : 380 , "tranX" : 190 , "tranY" : 625 , "rotate" : 270 ,  "arc_open": 'M0,-123.10745130262292L142.15224029763618,123.10745130262292 -142.15224029763618,123.10745130262292Z' },
    { "color" : "#e88a43", "transitionX" : 272 ,  "transitionY" : 572 , "tranX" : -500,  "tranY" : 397 , "rotate" : 90 ,  "arc_open": 'M0,-152.20440056461487L175.75050327565205,152.20440056461487 -175.75050327565205,152.20440056461487Z' }
];
var group = '';
var setAnimationFlagTwo = 0;
var setAnimationFlagOne = 0;
var setAnimationFlagThree = 0;

function createSvg(service_animate, bg_image, class_name, sectionData) {
    var svg = d3.select('#'+service_animate+'')
        .append('svg')
        .attr('preserveAspectRatio','none')
        .attr('viewBox','0 0 800 750');
    var data = [1];
    for (var i = 0; i < 12; i++) {
            if(i == 11){
              var defs = svg.append("defs");
              var pattern =  defs.append("pattern")
                  .attr('id',
                      function(){
                          return "artist-"+class_name+""
                      })
                  .attr("height", 1)
                  .attr("width", 1)
                  .attr("y",
                      function(){
                          return sectionData[i].transitionY
                      })
                  .attr("x",
                      function(){
                          return sectionData[i].transitionX
                      })
                  .attr("patternContentUnits", "objectBoundingBox")
                  .attr("preserveAspectRatio", "none")
                  .attr('patternTransform',
                      function() {
                          if(sectionData[i].rotate == 90)
                              return "rotate (" + 270 + ")  scale(1.1,1.2)"
                          else
                              return "rotate (" + 90 + ")  scale(1.1,1.2)"
                      });
              pattern.append("rect")
                  .attr("height", 500)
                  .attr("width", 500)
                  .attr("fill",
                      function(){
                          return sectionData[i].color;
                      });
              pattern.append("image")
                  .attr("width", 1)
                  .attr("height", 1)
                  .attr("xlink:href",
                      function(){
                          return bg_image;
                      });
            }

                group = svg.append('g')
                    .attr('class', class_name )
                    .style("opacity", function() {
                        return 1e-6
                    })
                    .attr('fill',
                        function() {
                            return sectionData[i].color;
                    })
                    .attr('transform',
                        function() {
                            return "translate(" + (sectionData[i].tranX  ) + "," +  sectionData[i].tranY + ") rotate (" + ((sectionData[i].rotate) +300) + ")"
                        });
                    group.selectAll('path')
                        .data(data).enter()
                        .append('path')
                        .attr('d', function() {
                          return sectionData[i].arc_open;
                        })
                        .attr('transform',
                            function(d,i){
                                return "translate("+(i*38)+","+(20)+") ";
                            })
                        .attr('fill',
                            function(){
                                if(i == 11){
                                    return "url(#artist-"+class_name+")";
                                }
                            });
    }
}
createSvg('service_animate_one', 'images/bg-service-min-1.png' , 'triangle_one', attribute_triangle_one);
createSvg('service_animate_two', 'images/bg-service-min-2.png' , 'triangle_two', attribute_triangle_two);
createSvg('service_animate_three', 'images/bg-service-min-3.png' , 'triangle_three', attribute_triangle_three);
    function animate(section , sectionData){
    d3.selectAll('.' + section + '')
        .data(sectionData)
        .style("opacity", 1e-6)
        .transition()
        .attr('transform',
            function(d, i) {if(i>=11) return "translate(" + d.transitionX + "," +  d.transitionY + ") rotate (" + (d.rotate) + ")"
                else return "translate(" + d.tranX + "," +  d.tranY + ") rotate (" + (d.rotate+300) + ")"
            })
        .duration(550)
        .style("opacity",
            function(d, i) {
              if(i==11) return 1
              else return 1e-6
            })
        .transition()
        .attr('transform',
            function(d, i) {
                if(i>=10) return "translate(" + d.transitionX + "," +  d.transitionY + ") rotate (" + (d.rotate) + ")"
                else return "translate(" + d.tranX + "," +  d.tranY + ") rotate (" + (d.rotate+300) + ")"
            })
        .duration(550)
        .style("opacity",
            function(d, i) {
                if(i>=10) return 1
                else return 1e-6
            })
        .transition()
        .attr('transform',
        function(d, i) {
            if(i>=8) return "translate(" + d.transitionX + "," +  d.transitionY + ") rotate (" + (d.rotate) + ")"
            else return "translate(" + d.tranX + "," +  d.tranY + ") rotate (" + (d.rotate+300) + ")"
        })
        .duration(550)
        .style("opacity",
        function(d, i) {
            if(i>=8) return 1
            else return 1e-6
        })
        .transition()
        .attr('transform',
        function(d, i) {
            if(i>=5) return "translate(" + d.transitionX + "," +  d.transitionY + ") rotate (" + (d.rotate) + ")"
            else return "translate(" + d.tranX + "," +  d.tranY + ") rotate (" + (d.rotate+300) + ")"
        })
        .duration(550)
        .style("opacity",
        function(d, i) {
            if(i>=5) return 1
            else return 1e-6
        })
        .transition()
        .attr('transform',
        function(d, i) {
            if(i>=1) return "translate(" + d.transitionX + "," +  d.transitionY + ") rotate (" + (d.rotate) + ")"
            else return "translate(" + d.tranX + "," +  d.tranY + ") rotate (" + (d.rotate+300) + ")"
        })
        .duration(550)
        .style("opacity",
        function(d, i) {
            if(i>=1) return 1
            else return 1e-6
        })
        .transition()
        .attr('transform',
        function(d, i) {
            return "translate(" + d.transitionX + "," +  d.transitionY + ") rotate (" + (d.rotate) + ")"
        })
        .duration(550)
        .style("opacity",
        function(d, i) {
            return 1
        });
};
var devBlocEleOne = document.getElementsByClassName('deployment-block')[0].offsetTop;
var devBlocEleTwo = document.getElementsByClassName('delivery-block')[0].offsetTop;
var devBlocEleThree = document.getElementsByClassName('development-block')[0].offsetTop;
function showTriangles() {
  var scroll = window.scrollY - (window.innerHeight * 0.4);

  if(scroll > devBlocEleOne  && setAnimationFlagOne == 0){
      setAnimationFlagOne = 1;
      animate('triangle_one', attribute_triangle_one);
  }
  if(scroll > devBlocEleThree && setAnimationFlagThree == 0){
      setAnimationFlagThree = 1;
      animate('triangle_three', attribute_triangle_three);
  }
  if(scroll > devBlocEleTwo && setAnimationFlagTwo == 0){
      setAnimationFlagTwo = 1;
      animate('triangle_two', attribute_triangle_two);
  }
};
window.addEventListener("scroll", function(){
  showTriangles();
});
showTriangles();
